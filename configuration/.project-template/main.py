#!/usr/bin/env python3
from ev3dev.ev3 import Button, Leds, Sound
from time import sleep

def blink_led():
    Leds.set_color(Leds.RIGHT, Leds.RED)
    Leds.set_color(Leds.LEFT, Leds.RED)
    sleep(1000)
    Leds.all_off() # turn of the Leds

def main():
    '''
    Start the program
    :return: None
    '''
    print("Execution of program started")
    # write your code here
    pass

if __name__ == '__main__':
    Sound.beep().wait()
    Sound.beep().wait()

    print("Please press the RIGHT button to start the program")
    button = Button()
    while not button.right:
        sleep(0.01)  # we just doze off for just a lil while

    main()
else:
    print('It is better to run the program from the terminal')
    print('Press the Right-Arrow button to start the program')
    main()
